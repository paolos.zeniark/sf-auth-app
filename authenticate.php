<?php

require_once './vendor/autoload.php';
require_once './config.php';

use Firebase\JWT\JWT;

// Set response content format
header('Content-Type: application/json; charset=utf-8');

// Retrieve request data
$reqData = json_decode(file_get_contents('php://input'), true);

// Set auth appID secret
$authAppId = getenv('AUTH_APP_ID');

// Validate appId
if (!isset($reqData['app_id']) || trim($reqData['app_id']) === '') {
    echo json_encode([
        'code' => 400,
        'message' => 'App ID cannot be blank!',
    ]); exit;
}

// Retrieve appId secret
$reqAppId = $reqData['app_id'];

// Verify appId secret
if ($authAppId != $reqAppId) {
    echo json_encode([
        'code' => 400,
        'message' => 'Server authentication failed!',
    ]); exit;
} 

// Retrieve account data from request
$reqAccountId = $reqData['account_id'];
$reqCompanyId = $reqData['company_id'];  

// Validate account id
if ($reqAccountId === '') {
    echo json_encode([
        'code' => 400,
        'message' => 'Account ID cannot be blank!',
    ]); exit;
}

// Validate account company id
if ($reqCompanyId === '') {
    echo json_encode([
        'code' => 400,
        'message' => 'Company ID cannot be blank!',
    ]); exit;
}

// Set jwt payload info
$payload = [
    'account_id' => $reqAccountId,
    'company_id' => $reqCompanyId
];

// Set jwt secret key
$key = getenv('JWT_KEY');

// Generate jwt using account info
$jwt = JWT::encode($payload, $key, getenv('JWT_ALGO'));

// Return generated jwt token
echo json_encode([
    'code' => 200,
    'message' => 'Successfully generated jwt!',
    'jwt' => $jwt,
]); exit;